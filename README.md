# 202012_velosiraptor

ADecemberToRemember - velosiraptor December 2020

- [TLE] - 19th December - `Hackerearth` -  - `Medium`
    - 40mins - understanding questions, strategizing data structure, refamiliarization with handling input
    - 20mins - write, testing, coding craft, implementing bool comprehension
    - Math[min, number theory], DS[list, string], conds[one-liner, bool], comprehension[list]

- 21st December - `Hackerrank` - LeapYearOrNot - `Medium`
    - 10mins - understanding questions, strategizing data structure, refamiliarization with question
    - 5mins - write, testing, coding craft, implementing concrete boolean
    - Math, boolean

- [NotSUbmitted] - 21st December - `Hackerrank` - FindAngle - `Medium`
    - 10mins - understanding questions, strategizing data structure, refamiliarization with question
    - 5mins - write, testing, coding craft, implementing concrete boolean
    - Math, boolean

- 21st December - `Hackerrank` - WordOrder - `Medium`
    - 20mins - understanding questions, strategizing data structure, refamiliarization with question
    - 25mins - write, testing, coding craft, implementing input, printing OrderedDict()
    - collections[OrderedDict], while loops, input, *dict.values()

- 21st December - `Hackerrank` - CompressTheWord - `Medium`
    - 20mins - understanding questions, strategizing data structure, understanding groupby()
    - 5mins - write, testing, coding craft, implementing input, printing OrderedDict()
    - itertools[groupby], unpacking[print(*list)]

- 22nd December - `Hackerrank` - AthleteSort - `Medium`
    - 10mins - understanding questions, strategizing data structure
    - 5mins - write, testing, coding craft, implementing input, printing by unpacking
    - DS[list], unpacking[print(*list)], sorted()