def min_and_xor_or(lst):
    calc = [[(lst[j] and lst[i]) ^ (lst[j] or lst[i]) for i in range(j+1, len(lst))] for j in range(len(lst)-1)]
    mn = (i for i in calc for i in i)
    return min(mn)

# T = int(input())
# for t in range(T):
#     N = int(input())
#     A = list(map(int, input().split()[:N+1]))
#     print(min_xor(A))

if __name__ == "__main__":
    tests = [
        [1, 2, 3, 4, 5], # 1
        [2, 4, 7] # 2
    ]
    for t in tests:
        print(min_and_xor_or(t))